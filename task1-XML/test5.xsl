<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/event">
        <html>
            <head>
                <title>Event<xsl:value-of select="@id"/>
                </title>
                <link rel="stylesheet" href="D:\WoltersKluwer\task1-XML\test5.css" type="text/css"/>
            </head>
            <body>
                <h1 class="title">
                    <xsl:value-of select="heading[@type='title']/text()"/>
                </h1>
                <xsl:apply-templates/>
                <footer>
                    <xsl:value-of select="heading[@type='title-footer']/text()"/>
                </footer>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="heading"/>

    <xsl:template match="doc-level">
        <div class="{attribute()}">
            <xsl:apply-templates/>
        </div>
    </xsl:template>


    <xsl:template match="para">
        <p class="{@type}">
            <xsl:choose>
                <xsl:when test="@type='unordered-list'">
                    <ul type="{@style}">
                        <xsl:apply-templates/>
                    </ul>
                </xsl:when>
                <xsl:when test="@type='num'">
                    <xsl:if test="not(empty(child::num/text())) and following-sibling::h1">
                        <xsl:value-of select="num"/>-<xsl:value-of select="following-sibling::h1"/>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </p>
    </xsl:template>


    <xsl:template match="h1"/>

    <xsl:template match="list-item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>


    <xsl:template match="image">
        <img src="http://2017rik.pp.ua/wp-content/uploads/2016/09/137b23738415771722b9264916feaae3.jpg"
             alt="{@description}"/>
    </xsl:template>

    <xsl:template match="external-link">
        <a href="{@source}" target="_blank">link</a>
    </xsl:template>

    <xsl:template match="date">
        Data:
        <time>
            <xsl:value-of select="text()"/>
        </time>
    </xsl:template>

</xsl:stylesheet>
