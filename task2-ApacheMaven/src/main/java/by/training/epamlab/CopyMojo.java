package by.training.epamlab;


import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;

@Mojo(name = "copyFile")
public class CopyMojo extends AbstractMojo {

    @Parameter
    private File in;
    @Parameter
    private File out;

    public void execute() throws MojoExecutionException {
        try {
            FileUtils.copyFile(in, out);
        } catch (IOException e) {
            throw new IllegalArgumentException("Fail with file", e);
        }
        getLog().info("Copy completed.");
    }
}
