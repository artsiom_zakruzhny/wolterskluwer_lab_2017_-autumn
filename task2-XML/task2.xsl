<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:wkdoc="http://www.wkpublisher.com/xml-namespaces/document"
                xmlns:xhtml="http://www.w3.org/1999/xhtml">
    <xsl:output method="html"/>
    <xsl:template match="/atlas-document">
        <html>
            <head>
                <title>Task2</title>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="heading">
        <h5>
            <xsl:apply-templates/>
        </h5>
    </xsl:template>

    <xsl:template match="wkdoc:level">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="para">
        <p>
            <xsl:if test="not(empty(@align))">
                <xsl:attribute name="align">
                    <xsl:value-of select="./@align"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="cite-ref">
        <b style="color: blue">
            <xsl:apply-templates/>
        </b>
    </xsl:template>

    <xsl:template match="italic">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="bold">
        <b>
            <xsl:apply-templates/>
        </b>
    </xsl:template>

    <xsl:template match="xhtml:table">
        <table cellspacing="{@cellspacing}" cellpadding="{@cellpadding}" width="{@width}">
            <xsl:if test="not(empty(@border))">
                <xsl:attribute name="border">
                    <xsl:value-of select="./@border"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </table>
    </xsl:template>

    <xsl:template match="xhtml:col">
        <col span="{@span}">
            <xsl:if test="not(empty(@width))">
                <xsl:attribute name="width">
                    <xsl:value-of select="./@width"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </col>
    </xsl:template>

    <xsl:template match="xhtml:tr">
        <tr>
            <xsl:apply-templates/>
        </tr>
    </xsl:template>

    <xsl:template match="xhtml:td">
        <td>
            <xsl:if test="descendant::note[@type='cch-comment']">
                <xsl:attribute name="bgcolor" select="'grey'"/>
                <xsl:attribute name="style" select="'padding:20'"/>
            </xsl:if>
            <xsl:apply-templates/>
        </td>
    </xsl:template>

    <xsl:template match="unordered-list">
        <ul type="{@token}">
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="list-item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>

    <xsl:template match="footnote[descendant::para]">
        <xsl:variable name="footnote" select="para/text()"/>
        <sup>
            <a href="" title="{$footnote}">
                <xsl:value-of select="@num"/>
            </a>
        </sup>
    </xsl:template>

</xsl:stylesheet>