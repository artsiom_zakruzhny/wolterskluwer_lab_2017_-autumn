package by.epam.wklab.interfaces;

import by.epam.wklab.exceptions.IllegalSumException;
import by.epam.wklab.soap.structure.RequestAvg;
import by.epam.wklab.soap.structure.ResponseAvg;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface CalculatorSoap {
    @WebMethod
    ResponseAvg getAverage(@WebParam(name = "request", mode = WebParam.Mode.IN) RequestAvg request) throws IllegalSumException;
}
