package by.epam.wklab.handlers;

import by.epam.wklab.exceptions.DocumentServiceException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class DocumentExceptionMapper implements ExceptionMapper<DocumentServiceException> {
    @Override
    public Response toResponse(DocumentServiceException e) {
        return Response.status(501).entity(e.getMessage()).build();
    }
}
