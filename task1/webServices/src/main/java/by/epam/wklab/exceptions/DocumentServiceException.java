package by.epam.wklab.exceptions;

import java.io.Serializable;

public class DocumentServiceException extends Exception implements Serializable {

    public DocumentServiceException() {
    }

    public DocumentServiceException(String cause) {
        super(cause);
    }

    public DocumentServiceException(String cause, Exception exception) {
        super(cause, exception);
    }
}
