package by.epam.wklab.data;

import by.epam.wklab.beans.Chapter;
import by.epam.wklab.beans.Document;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class DocumentsDAO {
    private Map<Integer, Document> storage;

    public DocumentsDAO() {
        storage = new HashMap<>();
    }

    public Document getDocument(int id) {
        return storage.get(id);
    }

    public Document createDocument(Document doc) {
        return storage.put(doc.getId(), doc);
    }

    public Document updateDocument(int id, Document doc) {
        return storage.put(id, doc);
    }

    public Document deleteDocument(int id) {
        return storage.remove(id);
    }

    public Document getDocumentWithShortestChapter() {
        int shortestChapter = Integer.MAX_VALUE;
        Document neededDoc = null;
        Iterator<Document> iterator = storage.values().iterator();
        if (iterator.hasNext()) {
            shortestChapter = iterator.next().getChapters().get(0).getPagesNumber();
        }
        for (Document document : storage.values()) {
            for (Chapter chapter : document.getChapters()) {
                if (chapter.getPagesNumber() <= shortestChapter) {
                    shortestChapter = chapter.getPagesNumber();
                    neededDoc = document;
                }
            }
        }
        return neededDoc;
    }
}
