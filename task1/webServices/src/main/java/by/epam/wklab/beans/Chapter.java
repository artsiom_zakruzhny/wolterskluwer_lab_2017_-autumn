package by.epam.wklab.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Chapter {
    private int id;
    private int chapterNumber;
    private int pagesNumber;

    public Chapter() {
        super();
    }

    public Chapter(int id, int chapterNumber, int pagesNumber) {
        super();
        this.id = id;
        this.chapterNumber = chapterNumber;
        this.pagesNumber = pagesNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(int chapterNumber) {
        this.chapterNumber = chapterNumber;
    }

    public int getPagesNumber() {
        return pagesNumber;
    }

    public void setPagesNumber(int pagesNumber) {
        this.pagesNumber = pagesNumber;
    }

    @Override
    public String toString() {
        return id + "; " + chapterNumber + "; " + pagesNumber;
    }
}
