package by.epam.wklab.soap.structure;

import net.webservicex.ArrayOfDouble;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RequestAverage")
public class RequestAvg {
    private ArrayOfDouble array;

    public RequestAvg() {
    }

    public ArrayOfDouble getArray() {
        return array;
    }

    public void setArray(ArrayOfDouble array) {
        this.array = array;
    }
}
