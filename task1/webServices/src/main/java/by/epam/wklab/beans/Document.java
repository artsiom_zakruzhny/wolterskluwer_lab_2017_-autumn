package by.epam.wklab.beans;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Document {
    private int id;
    private String name;
    private List<Chapter> chapters;

    public Document() {
        super();
    }

    public Document(int id, String name, List<Chapter> chapters) {
        super();
        this.id = id;
        this.name = name;
        this.chapters = chapters;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    @Override
    public String toString() {
        return id + "; " + name + "; " + chapters;
    }
}
