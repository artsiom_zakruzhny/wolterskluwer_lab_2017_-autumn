package by.epam.wklab.soap.service;

import by.epam.wklab.exceptions.IllegalSumException;
import by.epam.wklab.interfaces.CalculatorSoap;
import net.webservicex.Statistics;
import net.webservicex.StatisticsSoap;
import by.epam.wklab.soap.structure.RequestAvg;
import by.epam.wklab.soap.structure.ResponseAvg;

import javax.xml.ws.Holder;


public class CalculatedServiceImpl implements CalculatorSoap {

    @Override
    public ResponseAvg getAverage(RequestAvg request) throws IllegalSumException {
        Statistics statistics = new Statistics();
        StatisticsSoap statisticsSoap = statistics.getStatisticsSoap();
        Holder<Double> average;
        Holder<Double> sum;
        statisticsSoap.getStatistics(request.getArray(), sum = new Holder<>(), average = new Holder<>(), new Holder<>(), new Holder<>(), new Holder<>());
        if (sum.value < 0) {
            throw new IllegalSumException("sum less than 0...");
        }
        ResponseAvg response = new ResponseAvg();
        response.setAverage(average.value);
        return response;
    }
}
