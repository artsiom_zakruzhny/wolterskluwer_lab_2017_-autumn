package by.epam.wklab.interfaces;

import by.epam.wklab.beans.Document;
import by.epam.wklab.exceptions.DocumentServiceException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface DocumentRest {
    @POST
    @Path("documents/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_XML)
    Response createDoc(Document doc); // post

    @GET
    @Path("documents/{id}")
    @Produces(MediaType.APPLICATION_XML)
    Response readDoc(@PathParam("id") int id); // get

    @PUT
    @Path("documents/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_XML)
    Response updateDoc(@PathParam("id") int id, Document doc); // put

    @DELETE
    @Path("documents/{id}")
    @Produces(MediaType.APPLICATION_XML)
    Response deleteDoc(@PathParam("id") int id); // delete

    @GET
    @Path("documentsWithShortestChapter/")
    @Produces(MediaType.APPLICATION_XML)
    Response getDocWithShortestChapter() throws DocumentServiceException; // get

    @GET
    @Path("documents/chapters")
    @Produces(MediaType.APPLICATION_XML)
    Response getChapters(@PathParam("id") int id) throws DocumentServiceException; // get
}
