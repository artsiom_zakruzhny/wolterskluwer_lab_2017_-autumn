package by.epam.wklab.soap.structure;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ResponseAverage")
public class ResponseAvg {
    private double average;

    public ResponseAvg() {
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
