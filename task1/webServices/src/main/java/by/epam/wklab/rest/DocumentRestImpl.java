package by.epam.wklab.rest;

import by.epam.wklab.beans.Document;
import by.epam.wklab.data.DocumentsDAO;
import by.epam.wklab.exceptions.DocumentServiceException;
import by.epam.wklab.interfaces.DocumentRest;

import javax.ws.rs.core.Response;

public class DocumentRestImpl implements DocumentRest {
    private DocumentsDAO storage = new DocumentsDAO();

    @Override
    public Response createDoc(Document doc) {
        storage.createDocument(doc);
        return Response.status(201).entity(doc).build();
    }

    @Override
    public Response readDoc(int id) {
        Document read = storage.getDocument(id);
        if (read == null) {
            return Response.status(204).build();
        }
        return Response.status(200).entity(read).build();
    }

    @Override
    public Response updateDoc(int id, Document doc) {
        Document updated = storage.updateDocument(id, doc);
        if (updated == null) {
            return Response.status(201).entity(doc).build();
        }
        return Response.status(204).build();
    }

    @Override
    public Response deleteDoc(int id) {
        storage.deleteDocument(id);
        return Response.status(204).build();
    }

    @Override
    public Response getDocWithShortestChapter() throws DocumentServiceException {
        Document documentWithShortestChapter = storage.getDocumentWithShortestChapter();
        if (documentWithShortestChapter == null) {
            return Response.status(204).build();
        }
        return Response.status(200).entity(documentWithShortestChapter).build();
    }

    @Override
    public Response getChapters(int id) throws DocumentServiceException {
        throw new DocumentServiceException("method not implemented...");
    }
}
