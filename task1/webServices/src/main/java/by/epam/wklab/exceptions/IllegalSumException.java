package by.epam.wklab.exceptions;

import javax.xml.ws.WebFault;

@WebFault(name = "IllegalSumException")
public class IllegalSumException extends Exception {
    public IllegalSumException() {
    }

    public IllegalSumException(String message) {
        super(message);
    }

    public IllegalSumException(String message, Exception cause) {
        super(message, cause);
    }
}
