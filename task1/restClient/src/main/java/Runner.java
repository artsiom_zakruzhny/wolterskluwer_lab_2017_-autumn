import by.epam.wklab.implementation.ClientImpl;

public class Runner {
    public static void main(String[] args) {
        ClientImpl client = new ClientImpl();
        System.out.println("GET Doc: " + client.getDocument(22));
        client.createDocument("{\n" +
                "    \"chapters\":\n" +
                "    [ \n" +
                "    \t{\n" +
                "    \t\t\"id\":\"1\",\n" +
                "    \t\t\"chapterNumber\": \"1\",\n" +
                "    \t\t\"pagesNumber\": \"22\"\n" +
                "    \t}\n" +
                "    ],\n" +
                "    \"id\": \"22\",\n" +
                "    \"name\": \"lab22\"\n" +
                "}");
        client.createDocument("{\n" +
                "    \"chapters\":\n" +
                "    [ \n" +
                "    \t{\n" +
                "    \t\t\"id\":\"1\",\n" +
                "    \t\t\"chapterNumber\": \"1\",\n" +
                "    \t\t\"pagesNumber\": \"22\"\n" +
                "    \t},\n" +
                "    \t{\n" +
                "    \t\t\"id\":\"2\",\n" +
                "    \t\t\"chapterNumber\": \"2\",\n" +
                "    \t\t\"pagesNumber\": \"44\"\n" +
                "    \t},\n" +
                "    \t{\n" +
                "    \t\t\"id\":\"3\",\n" +
                "    \t\t\"chapterNumber\": \"3\",\n" +
                "    \t\t\"pagesNumber\": \"8\"\n" +
                "    \t}\n" +
                "    ],\n" +
                "    \"id\": \"333\",\n" +
                "    \"name\": \"lab333\"\n" +
                "}");
        System.out.println("GET Doc: " + client.getDocument(22));
        System.out.println("GET Doc: " + client.getDocument(333));
        client.deleteDocument(22);
        System.out.println("GET Doc: " + client.getDocument(22));
        System.out.println("GET Chapter: " +client.getChapters());
        System.out.println("UPDATE Doc: " +client.updateDocument(333, "{\n" +
                "    \"chapters\":\n" +
                "    [ \n" +
                "    \t{\n" +
                "    \t\t\"id\":\"1\",\n" +
                "    \t\t\"chapterNumber\": \"1\",\n" +
                "    \t\t\"pagesNumber\": \"22\"\n" +
                "    \t}\n" +
                "    ],\n" +
                "    \"id\": \"333\",\n" +
                "    \"name\": \"newLab\"\n" +
                "}"));
        client.deleteDocument(123);
        System.out.println("UPDATE Doc: " +client.updateDocument(123, "{\n" +
                "    \"chapters\":\n" +
                "    [ \n" +
                "    \t{\n" +
                "    \t\t\"id\":\"1\",\n" +
                "    \t\t\"chapterNumber\": \"1\",\n" +
                "    \t\t\"pagesNumber\": \"5\"\n" +
                "    \t}\n" +
                "    ],\n" +
                "    \"id\": \"123\",\n" +
                "    \"name\": \"zaki\"\n" +
                "}"));
        System.out.println("GET Doc: " + client.getDocument(123));
        System.out.println("GET Doc: " + client.getDocument(333));
        System.out.println("GET Doc with shortest chapter: "+client.getDocumentWithShortestChapter());
    }
}
