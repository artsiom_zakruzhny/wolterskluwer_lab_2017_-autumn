package by.epam.wklab.implementation;

import by.epam.wklab.interfaces.Client;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class ClientImpl implements Client {

    public Object getDocument(int id) {
        String result = null;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(URL + id);
        try {
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = readResponseInputStream(entity.getContent());
            }
        } catch (IOException e) {
            throw new RuntimeException(URL + "don't work");
        }
        return result;
    }

    public boolean createDocument(String dataJson) {
        Object result = null;
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(URL);
        StringEntity data = new StringEntity(dataJson, "UTF-8");
        request.setHeader("Content-Type", "application/json");
        request.setEntity(data);
        try {
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = readResponseInputStream(entity.getContent());
            }
        } catch (IOException e) {
            throw new RuntimeException(URL + "don't work");
        }
        if (result != null) {
            return true;
        }
        return false;
    }

    public Object updateDocument(int id, String dataJson) {
        String result = null;
        HttpClient client = HttpClientBuilder.create().build();
            HttpPut request = new HttpPut(URL+id);
            StringEntity data = new StringEntity(dataJson, "UTF-8");
            request.setHeader("Content-Type", "application/json");
            request.setEntity(data);
            try {
                HttpResponse response = client.execute(request);
                HttpEntity entity = response.getEntity();
                if(entity!=null){
                    result = readResponseInputStream(entity.getContent());
                }
            } catch (IOException e) {
                throw new RuntimeException(URL + "don't work");
            }
        return result;
    }

    public boolean deleteDocument(int id) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpDelete request = new HttpDelete(URL + id);
        try {
            client.execute(request);
        } catch (IOException e) {
            throw new RuntimeException(URL + "don't work");
        }
        return true;
    }

    public Object getChapters() {
        String result = null;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(URL +"chapters");
        try {
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = readResponseInputStream(entity.getContent());
            }
        } catch (IOException e) {
            throw new RuntimeException(URL + "don't work");
        }
        return result;
    }

    public Object getDocumentWithShortestChapter() {
        final String URL = "http://127.0.0.1:8080/services/documentsRestService/documentsWithShortestChapter";
        String result = null;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(URL);
        try {
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = readResponseInputStream(entity.getContent());
            }
        } catch (IOException e) {
            throw new RuntimeException(URL + "don't work");
        }
        return result;
    }

    private static String readResponseInputStream(InputStream inputStream) {
        Scanner scanner = null;
        String result = null;
        try {
            scanner = new Scanner(inputStream);
            StringBuilder builder = new StringBuilder();
            while (scanner.hasNext()) {
                builder.append(scanner.nextLine());
            }
            result = builder.toString();
        } finally {
            scanner.close();
        }
        return result;
    }
}
