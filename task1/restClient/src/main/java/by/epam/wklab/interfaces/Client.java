package by.epam.wklab.interfaces;

public interface Client {
    String URL = "http://127.0.0.1:8080/services/documentsRestService/documents/";

    Object getDocument(int id);

    boolean createDocument(String data);

    Object updateDocument(int id, String data);

    boolean deleteDocument(int id);

    Object getChapters();

    Object getDocumentWithShortestChapter();
}
